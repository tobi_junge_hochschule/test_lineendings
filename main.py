#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()

def move(x_start, y_start, x_target, y_target):
    


# Write your program here.
while True:
    for x in range(0,108):
        ev3.screen.draw_box(0 + x, 0, 20 + x, 20, fill=True,color=Color.BLACK)
        wait(50)
        ev3.screen.clear()

    for x in range(0,108):
        ev3.screen.draw_box(108 - x, 0 + x, 128 - x, 20 + x, fill=True, color=Color.BLACK)
        wait(50)
        ev3.screen.clear()

    for x in range(0,108):
        ev3.screen.draw_box(0 + x, 108, 20 + x, 128, fill=True,color=Color.BLACK)
        wait(50)
        ev3.screen.clear()

    for x in range(0,108):
        ev3.screen.draw_box(108 - x, 128 - x, 128 - x, 108 - x, fill=True, color=Color.BLACK)
        wait(50)
        ev3.screen.clear()